﻿/**
 * Created by bianca on 18/03/2015.
 */
$(document).ready(function () {


    //Check window width on resize and based on the width show the right menu
    //$(window).resize(function () {

    //    var width = $(window).width();
    //    var menuXs = $('.menu-xs');
    //    var menuLg = $('.menu-lg');

    //    if (width > 1200) {
    //        menuLg.show();
    //        menuXs.addClass('hide');
    //    } else {
    //        menuLg.addClass('hide');
    //        menuXs.removeClass('hide');
    //    }
    //    console.log(width)

    //});

    //Set the div height based on another div height
    $('.col-19').height($('.col-20').height());
    $('.col-19-').height($('.col-20-').height());
    $('.col-21').height($('.col-22').height());

    // function chekPosition () {
    //    if (Modernizr.mq('(min-width: 1200px)')) {


    // }




    //Silck Carousel News
    //$(document).ready(function (){
    //    $('.multiple-items').slick({
    //        arrows: true,
    //        infinite: true,
    //        focusOnSelect: true,
    //        speed: 1000,
    //        slidesToShow: 4,
    //        slidesToScroll: 4,
    //        responsive: [
    //            {
    //                breakpoint: 1024,
    //                settings: {
    //                    slidesToShow: 3,
    //                    slidesToScroll: 3,
    //                    infinite: true,
    //                    dots: true
    //                }
    //            },
    //            {
    //                breakpoint: 600,
    //                settings: {
    //                    slidesToShow: 2,
    //                    slidesToScroll: 2
    //                }
    //            },
    //            {
    //                breakpoint: 480,
    //                settings: {
    //                    slidesToShow: 1,
    //                    slidesToScroll: 1
    //                }
    //            }
    //            // You can unslick at a given breakpoint now by adding:
    //            // settings: "unslick"
    //            // instead of a settings object
    //        ]
    //    });
    //
    //});


    ////Parallax Js
    //jQuery(window).trigger('resize').trigger('scroll');
    //iosFix = true;
    //$('#parallaxJumbo').parallax({imageSrc: '../bg1.jpg'});
    //



    /*!
     * classie v1.0.1
     * class helper functions
     * from bonzo https://github.com/ded/bonzo
     * MIT license
     *
     * classie.has( elem, 'my-class' ) -> true/false
     * classie.add( elem, 'my-new-class' )
     * classie.remove( elem, 'my-unwanted-class' )
     * classie.toggle( elem, 'my-class' )
     */

    /*jshint browser: true, strict: true, undef: true, unused: true */
    /*global define: false, module: false */

    (function (window) {

        'use strict';

        // class helper functions from bonzo https://github.com/ded/bonzo

        function classReg(className) {
            return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
        }

        // classList support for class management
        // altho to be fair, the api sucks because it won't accept multiple classes at once
        var hasClass, addClass, removeClass;

        if ('classList' in document.documentElement) {
            hasClass = function (elem, c) {
                return elem.classList.contains(c);
            };
            addClass = function (elem, c) {
                elem.classList.add(c);
            };
            removeClass = function (elem, c) {
                elem.classList.remove(c);
            };
        }
        else {
            hasClass = function (elem, c) {
                return classReg(c).test(elem.className);
            };
            addClass = function (elem, c) {
                if (!hasClass(elem, c)) {
                    elem.className = elem.className + ' ' + c;
                }
            };
            removeClass = function (elem, c) {
                elem.className = elem.className.replace(classReg(c), ' ');
            };
        }

        function toggleClass(elem, c) {
            var fn = hasClass(elem, c) ? removeClass : addClass;
            fn(elem, c);
        }

        var classie = {
            // full names
            hasClass: hasClass,
            addClass: addClass,
            removeClass: removeClass,
            toggleClass: toggleClass,
            // short names
            has: hasClass,
            add: addClass,
            remove: removeClass,
            toggle: toggleClass
        };

        // transport
        if (typeof define === 'function' && define.amd) {
            // AMD
            define(classie);
        } else if (typeof exports === 'object') {
            // CommonJS
            module.exports = classie;
        } else {
            // browser global
            window.classie = classie;
        }

    })(window);

});