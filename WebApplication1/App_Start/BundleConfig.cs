﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication1
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/parallax").Include(
                   "~/Scripts/parallax.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/foundation").Include(
            //         "~/Scripts/js-foundation/vendor/modernizr.js",
            //          "~/Scripts/js-foundation/vendor/jquery.js",
            //          "~/Scripts/js-foundation/vendor/fastclick.js",
            //          "~/Scripts/js-foundation/foundation",

            //         //"~/Scripts/js-foundation/foundation/foundation.alert.js",
            //         "~/Scripts/js-foundation/foundation/foundation.topbar.js",
            //         //"~/Scripts/js-foundation/foundation/foundation.dropdown.js",
            //         //"~/Scripts/js-foundation/foundation/foundation.orbit.js",
            //         "~/Scripts/js-foundation/foundation.min.js"));

            
            bundles.Add(new ScriptBundle("~/bundles/main-js").Include(
                    "~/Scripts/animation/hover_animation.js",
                     "~/Scripts/slick-carousel/slick.min.js",
                   "~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      "~/Content/bootstrap.min.css",
                      //"~/Content/foundation.min.css",
                      "~/Content/slick-carousel/slick-theme.css",
                      "~/Content/slick-carousel/slick.css",
                      "~/Content/stylesheets/css/ortana.css"));
        }
    }
}
